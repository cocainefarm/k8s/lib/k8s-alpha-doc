#!/usr/bin/env sh
set -euo pipefail

wd=$PWD

cd $1
for d in 1.*; do
    if [ -d $d ]; then
        docsonnet -o "$wd/content/$d" --urlPrefix "$d" "$d/main.libsonnet" &
    fi
done

wait

echo "$wd/content"
cd $wd/content

find . -type f | awk '{l=$1; gsub("-", "/", l); if (l!=$1) {print l} }' | xargs -I{} dirname {} | sort | uniq | xargs mkdir -p
find . -type f | awk '{l=$1; gsub("-", "/", l); if (l!=$1) {print "mv "$1" "l} }' | . /dev/stdin

for f in $(find . -type f); do
    dir=$(dirname $f)
    file=$(basename $f .md)

    if [ -d "$dir/$file" ] && [ "$file" != "README" ]; then
        mv "$f" "$dir/$file/_index.md"
        sed -i "2 i title: $file" "$dir/$file/_index.md"
        sed -i "2 i geekdocCollapseSection: true" "$dir/$file/_index.md"
    else
        sed -i "2 i title: $file" $f
    fi
done

for dir in 1.*; do
    if [ -d $dir ]; then
        cp ../fixed_content/_index.md "$dir"
    fi
done
